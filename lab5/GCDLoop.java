public class GCDLoop{

    public static void main(String[] args){

        int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		
        while (num1 != num2) {
        	if(num1 > num2)
                num1 -= num2;
            else
                num2 -= num1;
        }

        System.out.println("GCDLoop is = " + num2);
}

}

