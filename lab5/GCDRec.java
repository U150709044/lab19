public class GCDRec {

	public static void main(String[] args) {
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);

		int divisor = gcd(num1 > num2 ? num1 : num2 , num1 > num2 ? num2 : num1);
		System.out.println("GCDRec is = " + divisor);

	}

	private static int gcd(int num1, int num2) {
		if ( num2 == 0)
			return num1;
		
		return gcd(num2, num1 % num2);
		
		
	}

}

