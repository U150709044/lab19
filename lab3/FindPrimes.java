public class FindPrimes{
    public static void main(String[] args) {
        int user = Integer.parseInt(args[0]);

        for (int number = 2 ; number < user; number++) {
            boolean isPrime = true;            
            for (int divisior = 2; divisior < number; divisior++) {
                if (number % divisior == 0)
                    isPrime = false;
            }
            if (isPrime) {
                    System.out.print(number + " ");

            }
       }
    
    }
}

