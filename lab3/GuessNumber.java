import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); // Creates an object to read user input
		Random rand = new Random(); // Creates an object from Random class
		int number = rand.nextInt(100); // generates a number between 0 and 9
		int sum = 0;
		while (true) {
			System.out.println("Hi! I'm thinking of a number between 0 and 99.");
			System.out.println("If you want to exit the game press -1");
			System.out.print("Can you guess it: ");
			int guess = reader.nextInt();
			sum = sum+1;
			if (number == guess) {
				System.out.println("Congratulations !" + "You won after " + sum + " attempts!");
				
				break;
			} else if (guess == -1) {
				break;
			} else {
				System.out.println("Sorry please try again!");
				if (number > guess) {
					System.out.println("Hint : Try an Higher number");
				} else {
					System.out.println("Hint : Try a Lower number");
				}
			}

		}
	}
}
